# Login - Comunicacion de Redes Seguras#

Login utilizando doble factor de autentificacion. Combinando algo que el usuario sabe (usuario y contraseña) con algo que el usuario tiene (su celular).

### ¿Que tenemos acá? ###

+ Como me hago una copia y que tengo que instalar.
+ [Verlo funcionando](http://ardilla.com.ar:[PUERTO])

### Puesta en marcha del servidor ###

`$ mkdir /home/data/com_seguras`

`$ cd /home/data/com_seguras/`

`$ virtualenv venv` (apt-get install virtualenv)

`$ git clone git clone https://antolinij@bitbucket.org/antolinij/login-totp-comunicacion-de-redes-seguras.git`

`$ source venv/bin/activate`

`$ pip install -r requirements.txt`

### Puesta en marcha del cliente###

 [Instalar Google Authenticator en Android, IOS](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=es_419)

### Funcionamiento ###

*Levantar un servidor de prueba en Django.

`$ cd /home/data/com_seguras/redes/`

`$ python manage.py runserver 0.0.0.0:[PUERTO-LIBRE]`

+ Desde un cliente (navegador web) apuntar a la ip del servidor

+ Registrarse un usuario y luego ingresar al sitio

+ Activar doble factor y escanear el codigo con la app del celular (Google Authenticator)

+ Luego cada vez que ingresemos sesion tendremos que ingresar usuario, contraseña y el token que nos indicara la app, caduca cada 30 segundos.


### Como contribuir ###

+ tests

### Contacto ###

+ jony.antolini@gmail.com
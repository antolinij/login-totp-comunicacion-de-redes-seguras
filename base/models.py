from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
	#relacionamos el usuario con el perfil extendido
	user = models.OneToOneField(User)

	secret = models.CharField(max_length=100, blank=True)

	User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])

	def __unicode__(self):
		return self.user.username

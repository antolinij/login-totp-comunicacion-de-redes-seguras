from django.conf.urls import patterns, url
from base import views

urlpatterns= patterns('',
        url(r'^$', views.index, name='index'),
        url(r'^register/$', views.register, name='register'),
        url(r'^home/$', views.home, name='home'),
	url(r'^login/$', views.user_login, name='login'),
        url(r'^logout/$', views.user_logout, name='logout'),
	url(r'^activar/(?P<op>\w+)/', views.activar_doble_factor, name='activar'),	
	#url(r'^activar/new/$', views.activar_doble_factor, name='activar'),
	#url(r'^activar/persist/$', views.activar_doble_factor, name='activar'),
	)

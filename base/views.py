from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from base.forms import UserForm, UserProfileForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
import onetimepass as otp

def index(request):   
    #creamos un diccionario para pasar al template
    context_dict = {'variable': "Vengo de la vista y soy una variable"}
    if request.user.is_authenticated():
	#return render(request, 'base/home.html')
	return HttpResponseRedirect('/home/')    

    return render(request, 'base/index.html', context_dict)

@login_required(login_url='/')
def home(request):   

	if request.user.profile.secret == '0':

		#hacer todo lo correspondiente a nueva clave
		import random, base64
		secret= base64.b32encode(str(random.randrange(1000000000,9999999999)))
		#establecemos la variable secret como request para proximos cambios
		url='http://chart.apis.google.com/chart?cht=qr&chs=300x300&chl=otpauth%3A//totp/'+str(request.user.username)+'%40redes_seguras%3Fsecret%3D'+str(secret)
		#return render(request, 'base/activar.html', {'url': url, 'secret': secret})

		return render(request, 'base/home.html', {'url': url, 'secret': secret})
	else:
		return render(request, 'base/home.html', {'security': 'actived'})
        
def register(request):
	# A boolean value for telling the template whether the registration was successful.
	# Set to False initially. Code changes value to True when registration succeeds.
	registered = False

	# If it's a HTTP POST, we're interested in processing form data.
	if request.method == 'POST':
		# Attempt to grab information from the raw form information.
		# Note that we make use of both UserForm and UserProfileForm.
		user_form = UserForm(data=request.POST)
		#creamos una instancia de UserProfileForm para asociarle el secret
		profile_form = UserProfileForm()
		
		if not user_form.data['email'] == user_form.data['email_du']:
			return render(request, 'base/register.html', {'error_email': 'Por favor ingrese dos correos iguales'})

		# If the two forms are valid...
		if user_form.is_valid():
			# Save the user's form data to the database.
			user = user_form.save()

			# Now we hash the password with the set_password method.
			# Once hashed, we can update the user object.
			user.set_password(user.password)
			user.save()

			profile = profile_form.save(commit=False)
			profile.user = user
			profile.secret= 0	

			#guardamos el profile asociado al user
			profile.save()

			# Update our variable to tell the template registration was successful.
			registered = True
			return render(request, 'base/index.html', {'register_msg': 'Registro satisfactorio, por favor inicie sesion'} )
		else:
			return render(request, 'base/register.html', {'register_msg': 'Error en campos (puede ser que el usuario ya exista)'})


	else:
		user_form = UserForm()
		profile_form = UserProfileForm()

		return render(request, 'base/register.html',{'user_form': user_form, 'profile_form': profile_form, 'registered': registered} )

def user_login(request):
	token =0
	if request.user.is_authenticated():
		return HttpResponseRedirect('/home/')
	if request.method == 'POST':
        	#capturamos los datos del formulario de inicio de sesion
        	username = request.POST['username']
        	password = request.POST['password']
        	token = request.POST['token']
	
		user = authenticate(username=username, password=password)

		if user:
			secret= user.profile.secret        
			#validamos el token y si no enviamos un mensaje de error
			is_valid= otp.valid_totp(token, secret)
			if is_valid == False and secret != '0':
				return render(request, 'base/index.html', {'error_login': 'El token ingresado no es valido', 'secret': secret})	
    			if user.is_active:
                		login(request, user)
                		return HttpResponseRedirect('/home/')
           	 	else:
                		return HttpResponse("Tu usuario esta desactivado")
        	else:
        		#print "Login invalido, detalles: {0}, {1}".format(username, password)
            		#return HttpResponse("Login invalido")
            		return render(request, 'base/index.html', {'error_login': 'Hubo un error en el login'})
	else:
        	if not request.user.is_authenticated():
           		return render(request, 'base/index.html', {})
        	else:
            		return HttpResponseRedirect('/home/') 

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/')

@login_required
def activar_doble_factor(request, op=None):

	#si es post almaceno el secret y mando todo al index
	if request.method== 'POST':
		
		secret= request.POST['secret']
		user= request.user

		profile= user.profile
                profile.secret= secret
                profile.save()
                user.save()

		return render(request, 'base/home.html', {'security': 'activated'})
